from airflow import DAG
from airflow.datasets import Dataset
from airflow.decorators import task

from datetime import datetime

my_file = Dataset('/tmp/my_file.txt')

with DAG(
    dag_id='producer',
    start_date=datetime(2022, 12, 1),
    schedule='@daily',
    catchup=False
) as dag:

    @task(outlets=[my_file])
    def update_dataset():
        with open(my_file.uri, 'a+') as f:
            f.write('update from producer')

    update_dataset()