from airflow import DAG
from airflow.datasets import Dataset
from airflow.decorators import task

from datetime import datetime

my_file = Dataset('/tmp/my_file.txt')

with DAG(
    dag_id='consumer',
    start_date=datetime(2022, 12, 1),
    schedule=[my_file],
    catchup=False
) as dag:

    @task
    def read_dataset():
        with open(my_file.uri, 'r') as f:
            print(f.read())

    read_dataset()
